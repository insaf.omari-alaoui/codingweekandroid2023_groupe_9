package com.example.atry;



import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.atry.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class MainActivity extends Activity {

    private Button buttonLogin;
    private static final int REQUEST_IMAGE_CAPTURE = 1;
    public static final String EXTRA_IMAGE_BITMAP = "extra_image_bitmap";
    private Uri imageUri;
    private MediaPlayer mediaPlayer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        buttonLogin = findViewById(R.id.buttonLogin);
        mediaPlayer = MediaPlayer.create(this, R.raw.entertainer);
        mediaPlayer.start(); // Start playback when the activity is created
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lancerActiviteCamera();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mediaPlayer != null) {
            mediaPlayer.release();
        }


    }



    private void lancerActiviteCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Bitmap imageBitmap;
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            // L'image a été capturée avec succès
            Bundle extras = data.getExtras();
            imageBitmap = (Bitmap) extras.get("data");
            // Sauvegarder le bitmap sur le stockage externe
            imageUri = sauvegarderBitmap(imageBitmap);
            // Passer l'image capturée à une autre activité
            lancerAutreActiviteAvecImage(imageBitmap);
        }
    }
    private Uri sauvegarderBitmap(Bitmap imageBitmap) {
        // Sauvegarder le bitmap sur le stockage externe
        File fichierImage = new File(Environment.getExternalStorageDirectory(), "image.jpg");
        try {
            FileOutputStream fos = new FileOutputStream(fichierImage);
            imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.flush();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Uri.fromFile(fichierImage);
    }
    private void lancerAutreActiviteAvecImage (Bitmap imageBitmap){
        Intent intent = new Intent(this, ValiderImageActivity.class);
        intent.putExtra(EXTRA_IMAGE_BITMAP, imageBitmap);
        intent.putExtra("IMAGE_URI", imageUri.toString());
        startActivity(intent);
    }




}