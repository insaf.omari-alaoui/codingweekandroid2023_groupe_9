package com.example.atry;

import android.content.ClipData;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.io.ByteArrayOutputStream;

public class DescriptionActivity extends AppCompatActivity {

    private String imageUriString;
    private byte[] byteArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_description);
    }

    public void valider(View view) {
        sendEmail();

    }

    public void sendEmail() {
        EditText EditDescription = findViewById(R.id.textDescription);
        TextView EditPosition = findViewById(R.id.textPosition);
        String Description = EditDescription.getText().toString();
        String Position = EditPosition.getText().toString();
        // Adresse e-mail du destinataire
        String[] email = {"omarialaouiinsaf@gmail.com"};
        // Sujet de l'e-mail
        String subject = "Signalement de problème communautaire";
        // Corps de l'e-mail
        String body = "Bonjour,\n " + "\n" +
                "Je souhaite signaler un problème communautaire au campus de saclay.\n" + "\n" + "Description :" + Description + "\n" + "\n" + "Position :" + Position + "\n" + "\n" + "Cordialement";
        // Récupérez l'URI en tant que chaîne
        /*imageUriString = getIntent().getStringExtra("IMAGE_URI");
        // Récupérez le tableau de bytes de l'extra de l'intent
        byteArray = getIntent().getByteArrayExtra("image");
        // Convertissez la chaîne en URI
        Uri imageUri = Uri.parse(imageUriString);
        // Convertissez le tableau de bytes en Bitmap
        Bitmap receivedBitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        // Convertissez le Bitmap en tableau de bytes
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        receivedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        byte[] bitmapData = bytes.toByteArray();*/
        // Créer une intention pour envoyer l'e-mail
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_EMAIL, email);
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        // Ajoutez le tableau de bytes en tant que pièce jointe à l'intent
        //intent.putExtra("image",byteArray);
        //intent.putExtra(Intent.EXTRA_STREAM, imageEri)
        intent.putExtra(Intent.EXTRA_TEXT, body);
        intent.setType("message/rfc822"); // Spécifiez le type de données
        startActivity(Intent.createChooser(intent, "Choisir une application de messagerie"));


    }
}

