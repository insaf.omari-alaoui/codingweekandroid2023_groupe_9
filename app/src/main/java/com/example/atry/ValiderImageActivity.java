package com.example.atry;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

public class ValiderImageActivity extends AppCompatActivity {

    private String imageUriString;
    private byte[] byteArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_valider_image);

        ImageView imageView = findViewById(R.id.image_view);

        // Récupérer le Bitmap de l'intent
        Bitmap imageBitmap = getIntent().getParcelableExtra(MainActivity.EXTRA_IMAGE_BITMAP);
        // Récupérez l'URI en tant que chaîne
        imageUriString = getIntent().getStringExtra("IMAGE_URI");
        // Récupérez le tableau de bytes de l'extra de l'intent
        byteArray = getIntent().getByteArrayExtra("image");

        // Afficher le Bitmap dans l'ImageView
        if (imageBitmap != null) {
            imageView.setImageBitmap(imageBitmap);
        }

    }
    public void Prochaineétape(View view) {
        Intent versDescription = new Intent();
        versDescription.setClass(this,DescriptionActivity.class);
        versDescription.putExtra("IMAGE_URI", imageUriString);
        versDescription.putExtra("image", byteArray);
        startActivity(versDescription);
    }
}


